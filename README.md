# <img alt="SuperTuxParty Logo" src="assets/icons/icon-smallest.png" width="64" height="64" /> SuperTuxParty

A [free/libre](https://www.gnu.org/philosophy/free-sw.html) and
[open-source](https://opensource.org/docs/osd/) party game that is meant to
replicate the feel of games such as Mario Party.

![Mini-game Screenshot](screenshot.png)

## Download
You can download SuperTuxParty from [Itch.io](https://anti.itch.io/super-tux-party) or from the [official website](https://supertux.party/download).
## Engine
SuperTuxParty is built with the [Godot Engine](https://godotengine.org/).
The stable version, 3.2.2, is used.

## Issues
If you have ideas for mini-games, design improvements or have found a bug then
please report that under Issues.

## License
All code is licensed under the [GNU GPL V3.0](https://www.gnu.org/licenses/gpl.html) or, at your option, any later version.
See the [**LICENSE**](LICENSE) file for more information.

All other data such as art, sound, music, and etc. is released under a bunch
of different licenses.
See the [**LICENSE-ART**](LICENSE-ART.md) file , the [**LICENSE-MUSIC**](LICENSE-MUSIC.md) file and the [**LICENSE-SHADER**](LICENSE-SHADER.md) file for details.

## Translation
All translation is done on [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party/)

## Community
We also have a [subreddit](https://www.reddit.com/r/SuperTuxParty/)
for discussions about the project, and two Matrix rooms for
[development](https://matrix.to/#/#SuperTuxParty-Dev:matrix.org) and
[general](https://matrix.to/#/#SuperTuxParty-Extra:matrix.org) talking.
